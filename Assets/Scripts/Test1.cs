﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class Test1 {

	[Test]
	public void ForwardSpeedNotZero() {
		Assert.AreNotEqual(0, PlayerMovement.forwardForce);
	}

	[Test]
	public void SidewaysSpeedNotZero() {
		Assert.AreNotEqual(0, PlayerMovement.sidewaysForce);
	}

	[Test]
	public void DelayNotZero() {
		Assert.AreNotEqual(0f, GameManager.restartDelay);
	}

	[Test]
	public void GameStart() {
		GameObject gameObject=new GameObject();
		GameManager myGame = gameObject.AddComponent(typeof(GameManager)) as GameManager;
		Assert.AreNotEqual(true, myGame.gameHasEnded);
	}

	[Test]
	public void Refresh() {
		float time = Time.deltaTime;
		Assert.Greater (0.05, time);
	}
}