﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public bool gameHasEnded = false;

    public static float restartDelay = 1f;

    public GameObject completeLevelUI;

    public void CompleteLevel()
    {
		SceneManager.LoadScene(SceneManager.GetActiveScene() .name);
    }

	public void EndGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("GAME OVER!");
            Invoke("Restart", restartDelay); 
        }
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene() .name);
    }
}