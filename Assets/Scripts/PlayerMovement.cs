﻿using UnityEngine;
using UnityEngine.UI;
using NUnit.Framework;
using UnityEngine.TestTools;

public class PlayerMovement : MonoBehaviour {

	public Rigidbody rb;
    public static float forwardForce=1000;
    public static float sidewaysForce=1500;

	void Update () {
		rb.AddForce(0, 0, forwardForce * Time.deltaTime);
		if(Input.touchCount>0)
		{
			if (Input.mousePosition.x>Screen.width/2)
			{
				rb.AddForce(sidewaysForce * Time.deltaTime, 0, 0);
			}
			if (Input.mousePosition.x<Screen.width/2) {
				rb.AddForce (-sidewaysForce * Time.deltaTime, 0, 0);
			}
		}
        if(rb.position.y<-1f)
        {
            FindObjectOfType<GameManager>().EndGame();
        }
	}
		

}